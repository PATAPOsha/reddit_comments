import configparser

config = configparser.ConfigParser()
config.read('config.cfg')


BOT_NAME = 'reddit_comments'

SPIDER_MODULES = ['reddit_comments.spiders']
NEWSPIDER_MODULE = 'reddit_comments.spiders'

RETRY_TIMES = int(config['BOT_CONFIG']['retry_times'])
REDIRECT_ENABLED = False
RETRY_HTTP_CODES = [302, 500, 502, 503, 504, 522, 524, 408, 429]
ROBOTSTXT_OBEY = False

DOWNLOAD_TIMEOUT = int(config['BOT_CONFIG']['timeout'])
# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = int(config['BOT_CONFIG']['threads'])


# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
   'reddit_comments.middlewares.RedditCommentsSpiderMiddleware': 543,
}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'random_useragent.RandomUserAgentMiddleware': 400,
    'reddit_comments.middlewares.RedditCommentsDownloaderMiddleware': 543,
    'rotating_proxies.middlewares.RotatingProxyMiddleware': 610,
    # 'rotating_proxies.middlewares.BanDetectionMiddleware': 620,
}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'reddit_comments.pipelines.SqlitePipeline': 300,
}


# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

ROTATING_PROXY_LIST_PATH = 'proxies.txt'
USER_AGENT_LIST = "useragents.txt"
IMAGES_STORE = 'images'

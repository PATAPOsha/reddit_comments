# -*- mode: python ; coding: utf-8 -*-
import sys
import inspect
import scrapy
sys.path.extend(['C:\\Users\\pata\\PycharmProjects\\reddit_comments', 'C:/Users/pata/PycharmProjects/reddit_comments'])

import reddit_comments.spiders.comments_spider

block_cipher = None

def collect_source_files(modules):
    datas = []
    for module in modules:
        source = inspect.getsourcefile(module)
        dest = f"src.{module.__name__}"  # use "src." prefix
        datas.append((source, dest))
    return datas

source_files = collect_source_files([scrapy, reddit_comments.spiders.comments_spider])  # return same structure as `collect_data_files()`
source_files_toc = TOC((name, path, 'DATA') for path, name in source_files)

a = Analysis(['main.py'],
             pathex=['C:\\Users\\pata\\PycharmProjects\\reddit_comments'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(
    a.pure,
    a.zipped_data,
    source_files_toc,
    cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='reddit_comments.exe',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True , version='file_version.txt', icon='pata_ico.ico')

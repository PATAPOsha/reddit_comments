import time
import sqlite3

from reddit_comments.settings import config
from reddit_comments.items import CommentAuthor


class SqlitePipeline:
    def __init__(self):
        self.db_conn = sqlite3.connect(config["BOT_CONFIG"]["db_file"])
        self.cursor = self.db_conn.cursor()
        self.buffer = []

    def process_item(self, item: CommentAuthor, spider):
        self.buffer.append((item['username'], int(time.time())))
        if len(self.buffer) == 100:
            self.write_db(spider)
        return item

    def write_db(self, spider):
        self.cursor.executemany('INSERT INTO usernames VALUES(?,?);', self.buffer)
        self.db_conn.commit()
        spider.logger.info(f"Inserted {self.cursor.rowcount} items.")
        self.buffer = []

    def close_spider(self, spider):
        spider.logger.info("Saving on close_spider")
        self.write_db(spider)

        end_time = int(time.time()) + 1
        spider.logger.info(f"End ts: {end_time}")
        self.cursor.execute(f"SELECT username FROM usernames "
                            f"WHERE ts BETWEEN {spider.start_time} and {end_time};")
        with open(f'out_{end_time}.txt', 'w') as f:
            f.write("\n".join(x[0] for x in self.cursor.fetchall()))

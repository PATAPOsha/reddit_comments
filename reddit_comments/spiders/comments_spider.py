# =============================================================================
# Created By  : pataposha.dev@gmail.com
# Created Date: 04.01.2020
# =============================================================================
import time
import json
import scrapy
import sqlite3

from urllib.parse import urlencode
from bloom_filter import BloomFilter

from reddit_comments.items import CommentAuthor
from reddit_comments.settings import config


class CommentsSpider(scrapy.Spider):
    name = "comments_spider"

    def __init__(self, sub_reddits, *args, **kwargs):
        """

        :param sub_reddits:
        :param sort_method: str : 'hot'/'new'/'top'/'rising'
        :param args:
        :param kwargs:
        """
        self.start_time = int(time.time())
        self.sub_reddits = sub_reddits
        self.sort_method = config["BOT_CONFIG"]["sort_method"]
        self.posts_counter = dict()
        self.comments_counter = dict()

        self.max_posts = int(config["BOT_CONFIG"]["posts_num"])
        self.max_users = int(config["BOT_CONFIG"]["usernames_num"])

        self.bloom = None
        self.db_conn = sqlite3.connect(config["BOT_CONFIG"]["db_file"])
        self.cursor = self.db_conn.cursor()

        self._configure_db()
        self._configure_bloom()
        self.logger.info(f"Start ts: {self.start_time}")

        super().__init__(*args, **kwargs)

    def _configure_db(self):
        query = """
        CREATE TABLE IF NOT EXISTS usernames (
        username text PRIMARY KEY NOT NULL,
        ts integer
        );
        """
        self.cursor.execute(query)
        self.db_conn.commit()

    def _configure_bloom(self):
        max_elements = len(self.sub_reddits) * self.max_posts * self.max_users
        count_query = "SELECT Count(username) FROM usernames"
        self.cursor.execute(count_query)
        db_count = self.cursor.fetchone()[0]
        max_elements += db_count
        error_rate = 1 / 10**len(str(max_elements))
        self.bloom = BloomFilter(max_elements=max_elements, error_rate=error_rate)
        for row in self.cursor.execute("SELECT username FROM usernames"):
            username = row[0]
            self.bloom.add(username)
        self.db_conn.close()

    @property
    def api_headers(self):
        return {
            'Accept': '*/*',
            "Origin": "https://www.reddit.com",
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            "Referer": 'https://www.reddit.com',
        }

    def build_subreddit_url(self, reddit_id, after_cursor=None):
        """

        :param reddit_id:
        :param after_cursor:

        :return:
        """
        params = {
            'rtj': 'only',
            'redditWebClient': 'web2x',
            'app': 'web2x-client-production',
            'allow_over18': '1',
            'include': 'prefsSubreddit',
            'dist': '25',
            'layout': 'card',
            'sort': self.sort_method,
            'geo_filter': 'US'
        }
        if after_cursor:
            params['after'] = after_cursor
        if self.sort_method == 'top':
            params['t'] = 'month'     # 'hour'/'day'/'week'/'month'/'year'/'all'
        url = f'https://gateway.reddit.com/desktopapi/v1/subreddits/{reddit_id}?' + urlencode(params)
        return url

    @staticmethod
    def build_post_comments_url(post_id, reddit_id):
        params = {
            'rtj': 'only',
            'emotes_as_images': 'true',
            'allow_over18': '1',
            'include': 'prefsSubreddit',
            'subredditName': reddit_id,
            'hasSortParam': 'false',
            'include_categories': 'true',
            'onOtherDiscussions': 'false',
        }
        url = f'https://gateway.reddit.com/desktopapi/v1/postcomments/{post_id}?' + urlencode(params)
        return url

    @staticmethod
    def build_more_comments_url(post_id):
        params = {
            'emotes_as_images': 'true',
            'rtj': 'only',
            'allow_over18': '1',
            'include': ''
        }
        url = f'https://gateway.reddit.com/desktopapi/v1/morecomments/{post_id}?' + urlencode(params)
        return url

    def start_requests(self):
        downloader_middlewares = self.crawler.engine.downloader.middleware.middlewares
        self.RetryMiddleware = [middleware for middleware in downloader_middlewares if "RetryMiddleware" in str(type(middleware))][0]
        for sub_reddit_url in self.sub_reddits:
            reddit_id = sub_reddit_url.rsplit('/', 1)[-1]
            self.posts_counter[reddit_id] = 0
            url = self.build_subreddit_url(reddit_id)
            yield scrapy.Request(
                url=url,
                callback=self.process_posts,
                headers=self.api_headers,
                meta={"reddit_id": reddit_id}
            )

    def process_posts(self, response: scrapy.http.HtmlResponse):
        reddit_id = response.meta['reddit_id']
        json_data = json.loads(response.text)
        for post_id in json_data.get('postIds'):
            if self.posts_counter[reddit_id] < self.max_posts:
                self.comments_counter[post_id] = 0
                url = self.build_post_comments_url(post_id, reddit_id)
                self.posts_counter[reddit_id] += 1
                yield scrapy.Request(
                    url=url,
                    callback=self.process_comments,
                    headers=self.api_headers,
                    meta={"reddit_id": reddit_id, "post_id": post_id}
                )

        if self.posts_counter[reddit_id] >= self.max_posts:
            self.logger.info(f'Posts limit reached for reddit_id: {reddit_id}')
            return

        if json_data.get('postIds'):
            last_post = json_data['postIds'][-1]
            next_url = self.build_subreddit_url(reddit_id, after_cursor=last_post)
            yield scrapy.Request(
                url=next_url,
                callback=self.process_posts,
                headers=self.api_headers,
                meta={"reddit_id": reddit_id}
            )

    def process_comments(self, response: scrapy.http.HtmlResponse):
        reddit_id = response.meta['reddit_id']
        post_id = response.meta['post_id']
        json_data = json.loads(response.text)

        for comment_id, comment_data in json_data.get('comments', {}).items():
            username = comment_data['author']
            if username not in self.bloom:
                self.comments_counter[post_id] += 1
                self.bloom.add(username)
                item = CommentAuthor(username=username)
                yield item
            else:
                self.logger.info(f'Duplicate: {username} (reddit_id: {reddit_id}, post_id: {post_id})')

        if self.comments_counter[post_id] >= self.max_users:
            self.logger.info(f'Comments limit reached for post_id: {post_id}')
            return

        for _, more_comment in json_data.get('moreComments', {}).items():
            token = more_comment['token']
            more_url = self.build_more_comments_url(post_id)
            yield scrapy.http.JsonRequest(
                url=more_url,
                method='POST',
                callback=self.process_comments,
                headers=self.api_headers,
                data={"token": token},
                meta={"reddit_id": reddit_id, "post_id": post_id}
            )

# =============================================================================
# Created By  : pataposha.dev@gmail.com
# Created Date: 30.11.2020
# =============================================================================

import os

# EXTRA imports required for Pyinstaller
import random_useragent
import rotating_proxies
import rotating_proxies.middlewares
import rotating_proxies.policy
import reddit_comments
import reddit_comments.spiders
import reddit_comments.middlewares
import reddit_comments.items
import reddit_comments.pipelines
import reddit_comments.settings

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from reddit_comments.spiders.comments_spider import CommentsSpider

os.environ["SCRAPY_SETTINGS_MODULE"] = "reddit_comments.settings"

with open('urls.txt') as f:
    urls = [x.strip() for x in f.readlines()]

process = CrawlerProcess(get_project_settings())

process.crawl(CommentsSpider, urls)
process.start()
